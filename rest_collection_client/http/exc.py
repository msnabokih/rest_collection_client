from rest_collection_client.exc import ClientError

__all__ = [
    'ClientHttpError',
    'ClientHttpRequestError',
    'ClientHttpGetRequestError',
    'ClientHttpUnauthorizedGetRequestError',
]


class ClientHttpError(ClientError):
    """Root http client exception."""


class ClientHttpRequestError(ClientHttpError):
    """Http request error."""


class ClientHttpGetRequestError(
    ClientHttpRequestError
):
    """Http GET request error."""


class ClientHttpUnauthorizedGetRequestError(
    ClientHttpGetRequestError
):
    """Http GET unauthorized request error."""
