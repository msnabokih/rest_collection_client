from typing import Any, Hashable, Iterator, Mapping, Sequence

__all__ = [
    'Map',
    'Tuple',
]


class Map(Mapping):
    """User-defined map."""
    __slots__ = '_data',

    def __init__(self, data: Mapping) -> None:
        self._data = data

    def __getitem__(self, key: Hashable) -> Any:
        return self._data[key]

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator:
        return iter(self._data)


class Tuple(Sequence):
    """User-defined tuple."""
    __slots__ = '_data',

    def __init__(self, *data: Any) -> None:
        self._data = data

    def __getitem__(self, index: int) -> Any:
        return self._data[index]

    def __len__(self) -> int:
        return len(self._data)
