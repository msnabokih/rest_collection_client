from math import isnan

from rest_collection_client.join import RestCollectionJoinColumns, \
    RestCollectionJoinRule, RestCollectionJoinSequence
from rest_collection_client.response import RestCollectionResponse

_JOIN_SEQUENCE = RestCollectionJoinSequence(
    RestCollectionJoinRule(
        RestCollectionJoinColumns('alpha', 'id'),
        RestCollectionJoinColumns('beta', 'alpha_id')
    ),
    RestCollectionJoinRule(
        RestCollectionJoinColumns('beta', 'id_1', 'id_2'),
        RestCollectionJoinColumns('gamma', 'beta_id_1', 'beta_id_2')
    )
)


_DATA = RestCollectionResponse.from_raw_response({
    'alpha': [
        {'alpha.id': 1, 'alpha.name': 'alpha 1'}
    ],
    'beta': [
        {'beta.id_1': 1, 'beta.id_2': 2, 'beta.alpha_id': 1},
        {'beta.id_1': 3, 'beta.id_2': 4, 'beta.alpha_id': 1},
    ],
    'gamma': [
        {'gamma.id': 1, 'gamma.beta_id_1': 3, 'gamma.beta_id_2': 4}
    ]
})


def test_join():
    df = _JOIN_SEQUENCE.join(_DATA)

    # Replacing of NaNs (if it is needed) should occurs before join
    # operations in real usage case.
    data = [
        {
            k: None if isinstance(v, float) and isnan(v) else v
            for k, v in row.items()
        } for _, row in df.iterrows()
    ]

    assert data == [
        {'alpha.id': 1,
         'alpha.name': 'alpha 1',
         'beta.alpha_id': 1,
         'beta.id_1': 1,
         'beta.id_2': 2,
         'gamma.beta_id_1': None,
         'gamma.beta_id_2': None,
         'gamma.id': None},
        {'alpha.id': 1,
         'alpha.name': 'alpha 1',
         'beta.alpha_id': 1,
         'beta.id_1': 3,
         'beta.id_2': 4,
         'gamma.beta_id_1': 3.0,
         'gamma.beta_id_2': 4.0,
         'gamma.id': 1.0}
    ]
